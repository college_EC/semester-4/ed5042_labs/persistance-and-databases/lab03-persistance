package ie.ul.shuhupdaphone.gui;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.widget.TextView;

import ie.ul.shuhupdaphone.R;

public class InformationActivity extends Activity {

    TextView information;

    @Override
    public void onCreate(Bundle savedInstance){
        super.onCreate(savedInstance);
        setContentView(R.layout.activity_information);

        SharedPreferences myPrefs;
        String name;

        myPrefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());

        name = myPrefs.getString(AppPreferences.KEY_USERNAME, "");

        this.information = (TextView) findViewById(R.id.information);
        if(name.equals("")){
            this.information.setText(R.string.default_username_display_text);
        } else {
            this.information.setText(name);
        }
    }

}
